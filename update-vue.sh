#!/usr/bin/env bash
# install dependencies
yarn install
# create examples.ts
FILE="src/modules/examples.ts"
echo "/* eslint-disable */" > $FILE
echo "// !!! Automatically generated, do not edit !!!" >> $FILE
echo >> $FILE
echo "const EXAMPLES = [" >> $FILE
for i in `ls public/sequences`; do
	seq=`basename $i .json`
	echo "  '$seq'," >> $FILE
done
echo "]" >> $FILE
echo >> $FILE
echo "export default EXAMPLES" >> $FILE
# build application
yarn run build || exit 1
# check if we can update lebiniou-data
DATADIR="../lebiniou-data"
[ -d $DATADIR ] || exit
echo -n "Copying files to $DATADIR/vue... "
cp -r dist/* ../lebiniou-data/vue/
echo "done."
# create Makefile.am
echo -n "Creating $DATADIR/vue/sequences/Makefile.am... "
FILE="$DATADIR/vue/sequences/Makefile.am"
echo "# !!! Automatically generated, do not edit !!!" > $FILE
echo "sequencesdir = \$(datadir)/lebiniou/vue/sequences" >> $FILE
echo "FILES = \\" >> $FILE
for i in `ls public/sequences`; do
	echo -e "\t$i \\" >> $FILE
done
sed -i '$ s/\\$//' $FILE
sed -i '$ s/ $//' $FILE
echo "sequences_DATA = \${FILES}" >> $FILE
echo "EXTRA_DIST = \${FILES}" >> $FILE
echo "done."
