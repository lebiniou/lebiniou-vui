# lebiniou-vui

## Project setup
```
$ yarn install
```

### Generate files
```
$ ./update-vue.sh
```

### Compiles and hot-reloads for development
```
$ yarn run serve
```

### Compiles and minifies for production
```
$ yarn run build
```

### Lints and fixes files
```
$ yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
