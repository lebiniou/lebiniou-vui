import state from '@/state'
import getters from '@/state/getters'

let hubWs: any = null

const connectHub = (callback: Function) => {
  if (hubWs === null) {
    const url = getters.getHubWsUrl(state)

    console.log(`Connecting to ${url}`)
    hubWs = new WebSocket(url)

    hubWs.onopen = () => {
      console.info(`Connected to ${url}`)
      callback()
    }

    hubWs.onmessage = (message: any) => {
      console.log('onmessage', message.data)
    }

    hubWs.onclose = () => {
      console.log('Reconnecting...')
      connectHub(callback)
    }

    hubWs.onerror = () => {
      hubWs.close()
    }
  }
}

const disconnectHub = () => {
  if (hubWs) hubWs.close()
}

const sendHub = (message: string) => {
  console.log('sendHub', message)
  if (hubWs) hubWs.send(message)
}

export { connectHub, disconnectHub, sendHub }
