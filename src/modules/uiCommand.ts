import state from '@/state'
import { modifySequence, updateAuto, updateSelectedPlugin } from '@/state/mutations'

interface Payload {
  emitter: boolean
  result: any
  uiCommand: string
}

const handleUiCommand = (payload: Payload) => {
  const { uiCommand, result, emitter } = payload
  console.log('handleUiCommand', uiCommand, result, emitter)
  switch (uiCommand) {
    case 'UI_CMD_APP_TOGGLE_RANDOM_SCHEMES':
    case 'UI_CMD_APP_TOGGLE_RANDOM_SEQUENCES': {
      const { randomSchemes, randomSequences } = result
      const value = randomSchemes && randomSequences ? 'Mixed' : (randomSchemes ? 'Schemes' : (randomSequences ? 'Sequences' : 'Off'))
      state.auto.sequences.value = value
    }
      break

    case 'UI_CMD_APP_SET_AUTO_MODE':
      updateAuto(result)
      break

    case 'UI_CMD_APP_SET_BANDPASS':
      state.sequence.bandpass = result
      break

    case 'UI_CMD_APP_SET_DELAY':
      updateAuto(result)
      break

    case 'UI_CMD_APP_SET_FADE_DELAY':
      state.engine.fadeDelay = result.fadeDelay
      break

    case 'UI_CMD_APP_SET_VOLUME_SCALE':
      state.input.volumeScale = result.volumeScale / 1000
      break

    case 'UI_CMD_SEQ_SET_LAYER_MODE':
    case 'UI_CMD_SEQ_REORDER':
      modifySequence(result.sequence)
      break

    case 'UI_CMD_APP_SELECT_PLUGIN':
      updateSelectedPlugin(result)
      break

    case 'UI_CMD_APP_SET_MAX_FPS':
      state.engine.maxFps = result.maxFps
      break

    case 'UI_CMD_SELECT_ITEM': {
      const { item, colormap, image, mode } = result
      if (item === 'colormap') {
        state.sequence.colormap = colormap
      } else if (item === 'image') {
        state.sequence.image = image
      } else if (item === 'boundaryMode') {
        state.params3d.boundary = mode
      }
      state.sequence.modified = true
    }
      break

    case 'UI_CMD_COL_NEXT_N':
    case 'UI_CMD_COL_PREVIOUS_N':
      state.sequence.colormap = result.colormap
      state.sequence.modified = true
      break

    case 'UI_CMD_IMG_NEXT_N':
    case 'UI_CMD_IMG_PREVIOUS_N':
      state.sequence.image = result.image
      state.sequence.modified = true
      break

    case 'UI_CMD_SEQ_SET_PARAM_CHECKBOX_VALUE':
    case 'UI_CMD_SEQ_SET_PARAM_SELECT_VALUE':
    case 'UI_CMD_SEQ_SET_PARAM_PLAYLIST_VALUE':
    case 'UI_CMD_SEQ_SET_PARAM_SLIDER_VALUE':
      updateSelectedPlugin(result)
      state.sequence.modified = true
      break

    case 'UI_CMD_APP_SET_3D_ROTATION_AMOUNT':
    case 'UI_CMD_APP_SET_3D_ROTATION_FACTOR':
      state.params3d = result
      state.sequence.modified = true
      break

    case 'UI_CMD_TRACKBALL_ON_DRAG_MOVE':
    case 'UI_CMD_TRACKBALL_ON_DRAG_START':
    case 'UI_CMD_TRACKBALL_ON_MOUSE_WHEEL':
      break

    default:
      console.warn('Unhandled uiCommand result', uiCommand, result)
      break
  }

  if (emitter) {
    state.spin--
  }
}

export default handleUiCommand
