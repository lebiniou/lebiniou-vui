import state from '@/state'
import { updateAuto, modifySequence, updateSequence, updateSequences } from '@/state/mutations'

interface Payload {
  command: string
  result: any
  emitter: boolean
}

const handleCommand = (payload: Payload) => {
  const { command, result, emitter } = payload
  console.log('handleCommand', command, result, emitter)
  switch (command) {
    case 'CMD_APP_SWITCH_FULLSCREEN':
      state.screen.fullscreen = result.fullscreen
      break

    case 'CMD_APP_TOGGLE_AUTO_COLORMAPS':
    case 'CMD_APP_TOGGLE_AUTO_IMAGES':
      updateAuto(result)
      break

    case 'CMD_APP_RANDOMIZE_3D_ROTATIONS':
    case 'CMD_APP_TOGGLE_3D_ROTATIONS':
      state.params3d = result
      state.sequence.modified = true
      break

    case 'CMD_APP_FREEZE_INPUT':
      state.input.mute = result.mute
      break

    case 'CMD_APP_STOP_AUTO_MODES':
      state.auto.colormaps.value = false
      state.auto.images.value = false
      state.auto.sequences.value = 'Off'
      state.auto.webcams.value = false
      break

    case 'CMD_COL_NEXT':
    case 'CMD_COL_PREVIOUS':
    case 'CMD_COL_RANDOM':
      state.sequence.colormap = result.colormap
      state.sequence.modified = true
      break

    case 'CMD_IMG_NEXT':
    case 'CMD_IMG_PREVIOUS':
    case 'CMD_IMG_RANDOM':
      state.sequence.image = result.image
      state.sequence.modified = true
      break

    case 'CMD_SEQ_TOGGLE_LENS':
    case 'CMD_APP_TOGGLE_SELECTED_PLUGIN':
      modifySequence(result.sequence)
      break

    case 'CMD_APP_LOCK_SELECTED_PLUGIN':
      state.engine.lockedPlugin = result.lockedPlugin
      break

    case 'CMD_APP_RANDOM_SCHEME':
      updateSequence(result.sequence)
      break

    case 'CMD_APP_RANDOM_SEQUENCE':
      updateSequence(result.sequence)
      break

    case 'CMD_APP_FIRST_SEQUENCE':
    case 'CMD_APP_LAST_SEQUENCE':
    case 'CMD_APP_NEXT_SEQUENCE':
    case 'CMD_APP_PREVIOUS_SEQUENCE':
      updateSequence(result.sequence)
      break

    case 'CMD_APP_CLEAR_SCREEN':
    case 'CMD_APP_NEXT_WEBCAM':
    case 'CMD_APP_RANDOMIZE_SCREEN':
      break

    case 'CMD_SEQ_SAVE_BARE':
    case 'CMD_SEQ_SAVE_FULL':
      updateSequences(result)
      state.sequence.name = result.sequence.id
      state.sequence.modified = false
      break

    case 'CMD_SEQ_UPDATE_BARE':
    case 'CMD_SEQ_UPDATE_FULL':
      state.sequence.modified = false
      break

    case 'CMD_SEQ_RESET': {
      const newSequence = state.sequence
      newSequence.plugins = []
      state.sequence = newSequence
      state.sequence.modified = true
    }
      break

    default:
      console.warn('Unhandled command', command, result)
  }

  if (emitter) {
    state.spin--
  }
}

export default handleCommand
