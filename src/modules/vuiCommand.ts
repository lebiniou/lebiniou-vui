import state from '@/state'
import { updateAuto, updateSequence, updateSequences } from '@/state/mutations'

interface Payload {
  emitter: boolean
  result: any
  vuiCommand: string
}

const handleVuiCommand = (payload: Payload) => {
  const { vuiCommand, result, emitter } = payload
  console.log('handleVuiCommand', vuiCommand, result, emitter)
  switch (vuiCommand) {
    case 'VUI_CONNECT': {
      const {
        versions, engine, input, data, screen, sequence,
        auto, shortcuts, params3d, allSequences
      } = result

      state.versions = versions
      state.engine = engine
      state.input = input
      state.screen = screen
      state.data = data
      allSequences.reverse()
      for (const seq of allSequences) {
        seq.newName = seq.name
        seq.marked = seq.rename = false
      }
      state.allSequences = allSequences
      state.params3d = params3d
      updateSequence(sequence)
      state.shortcuts = shortcuts
      state.auto = auto
      state.connected = true
    }
      break

    case 'VUI_DELETE_SEQUENCES':
      updateSequences(result)
      state.data.sequences = result.sequences
      break

    case 'VUI_GENERATE_RANDOM': {
      const { randomSchemes, randomSequences } = result
      const value = randomSchemes && randomSequences ? 'Mixed' : (randomSchemes ? 'Schemes' : (randomSequences ? 'Sequences' : 'Off'))
      state.auto.sequences.value = value
    }
      break

    case 'VUI_LOCK':
      switch (result.lock) {
        case 'colormap':
          state.engine.lockColormap = result.value
          break

        case 'image':
          state.engine.lockImage = result.value
          break

        case 'webcam':
          state.engine.lockWebcam = result.value
          break
      }
      break

    case 'VUI_POST_SEQUENCE':
      updateSequence(result)
      break

    case 'VUI_RENAME_SEQUENCE':
      if (result.error) {
        alert(`Error renaming sequence: ${result.error}`)
        state.allSequences[result.index].name = state.allSequences[result.index].newName = result.oldName
      } else {
        updateSequences(result)
      }
      break

    case 'VUI_RESET_3D':
      state.params3d = result
      state.sequence.modified = true
      break

    case 'VUI_SELECT_WEBCAM':
      break

    case 'VUI_SELECTOR_CHANGE':
      if (result.colormap) {
        state.sequence.colormap = result.colormap
        state.sequence.modified = true
      } else if (result.image) {
        state.sequence.image = result.image
        state.sequence.modified = true
      }
      break

    case 'VUI_SHORTCUT': {
      const { colormap, image, index, id, cleared } = result
      if (cleared === 'colormap') {
        state.shortcuts.colormaps[index] = null
      } else if (cleared === 'image') {
        state.shortcuts.images[index] = null
      } else if (colormap) {
        state.sequence.colormap = colormap
        state.sequence.modified = true
        if (id) {
          state.shortcuts.colormaps[index] = id
        }
      } else if (image) {
        state.sequence.image = image
        state.sequence.modified = true
        if (id) {
          state.shortcuts.images[index] = id
        }
      }
    }
      break

    case 'VUI_TOGGLE':
      updateAuto(result)
      break

    case 'VUI_USE_SEQUENCE':
      updateSequence(result.sequence)
      break

    default:
      console.warn('Unhandled vuiCommand result', vuiCommand, result)
      break
  }
  if (emitter) {
    state.spin--
  }
  if (vuiCommand === 'VUI_CONNECT') {
    state.spin = 0
  }
}

export default handleVuiCommand
