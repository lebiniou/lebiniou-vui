const capitalize = (str: string | null) => str ? str[0].toUpperCase() + str.substring(1) : null

const MAXLEN = 15
const shorten = (str: string) => str.substring(0, MAXLEN) + ((str.length > MAXLEN) ? '...' : '')

const rescalePage = function (someClass: string) {
  console.info(`Rescaling the page (${someClass} class)`)

  const footerDiv = document.getElementsByClassName('lb-footer-container')[0] as HTMLElement
  let footerHeight = 0
  if (footerDiv) {
    footerHeight = footerDiv.offsetHeight
  }

  const containerDiv = document.getElementsByClassName(someClass)[0] as HTMLElement
  if (!containerDiv) { // should not happen anymore, so making this an error
    console.error(`Element for class '${someClass}' not found`)
    return
  }

  containerDiv.style.display = 'block'

  const currentWidth = containerDiv.offsetWidth
  const currentHeight = containerDiv.offsetHeight + footerHeight

  const availableHeight = window.innerHeight
  const availableWidth = window.innerWidth

  const scaleX = availableWidth / currentWidth
  const scaleY = availableHeight / currentHeight

  const translationX = Math.round((availableWidth - (currentWidth * scaleX)) / 2)
  const translationY = Math.round((availableHeight - (currentHeight * scaleY)) / 2)

  const style = document.createElement('style')
  style.innerHTML =
    `.${someClass} {
      position: fixed;
      left: 0px;
      top: 0px;
      transform: translate(${translationX}px, ${translationY}px) scale3d(${scaleX}, ${scaleY}, 1);
      transform-origin: 0 0;
    }`

  const s = document.querySelector('script')
  if (s !== null && s.parentNode !== null) {
    s.parentNode.insertBefore(style, s)
  }
}

export { capitalize, rescalePage, shorten }
