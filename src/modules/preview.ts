import state from '@/state'
import getters from '@/state/getters'

const PREVIEW_WIDTH = 480
const PREVIEW_HEIGHT = 270

let previewWs: any = null
let connected: any = null
let setFrame: any = null
let watchdog: number = -1

const connect = (connectedCb: Function, setFrameCb: Function) => {
  connected = connectedCb
  setFrame = setFrameCb

  if (previewWs !== null) {
    connected()
  } else {
    console.log(`Connecting to ${getters.getPreviewUrl(state)}`)
    previewWs = new WebSocket(getters.getPreviewUrl(state))

    previewWs.onopen = () => {
      console.info(`Connected to ${getters.getPreviewUrl(state)}`)
      connected !== null ? connected() : previewWs.close()
    }

    previewWs.onmessage = (message: any) => {
      clearTimeout(watchdog)
      if (setFrame !== null) {
        setFrame(message.data)
      }
    }

    previewWs.onclose = () => {
      previewWs = null
      if (connected !== null) {
        console.log('Reconnecting...')
        connect(connected, setFrame)
      }
    }

    previewWs.onerror = () => {
      previewWs.close()
    }
  }
}

const getFrame = (fullscreen = false, timeout = false) => {
  if (previewWs !== null && previewWs.readyState === 1) {
    watchdog = setTimeout(() => getFrame(fullscreen, true), 1000)
    if (fullscreen) {
      previewWs.send(JSON.stringify({ getFrame: null }))
    } else {
      previewWs.send(JSON.stringify({ getFrame: { width: PREVIEW_WIDTH, height: PREVIEW_HEIGHT } }))
    }
  }
}

const disconnect = () => {
  connected = setFrame = null
  clearTimeout(watchdog)
}

const close = () => {
  if (previewWs) previewWs.close()
}

export { connect, disconnect, getFrame, close, PREVIEW_WIDTH, PREVIEW_HEIGHT }
