import state from '@/state'

import getters from '@/state/getters'
import { updateSequence } from '@/state/mutations'

import handleCommand from '@/modules/command'
import handleUiCommand from '@/modules/uiCommand'
import handleVuiCommand from '@/modules/vuiCommand'
import { close as closePreviewWs } from '@/modules/preview'

let ws: any = null

const connect = () => {
  if (ws !== null) {
    console.info('Already connected')
  } else {
    const wsUrl = getters.getWsUrl(state)
    console.info(`Connecting to ${wsUrl}`)
    ws = new WebSocket(wsUrl)

    ws.onopen = () => {
      console.info(`Connected to ${wsUrl}`)
      vuiCommand('VUI_CONNECT')
    }

    ws.onmessage = (message: any) => {
      handle(JSON.parse(message.data))
    }

    ws.onclose = () => {
      state.connected = false
      console.info('Reconnecting...')
      ws = null
      connect()
    }

    ws.onerror = () => {
      ws.close()
    }
  }
}

const handle = (payload: any) => {
  if (payload.ping) {
    // FIXME: current ping returns 'maxFps' which we don't use
    const { clients, currentWebcam, fps, uptime } = payload.ping
    state.ping = { clients: clients, currentWebcam: currentWebcam, fps: fps, uptime: uptime }
    state.fpsLog.shift()
    state.fpsLog.push(fps)
  } else if (payload.colormap) {
    state.sequence.colormap = payload.colormap
  } else if (payload.image) {
    state.sequence.image = payload.image
  } else if (payload.sequence) {
    updateSequence(payload.sequence)
  } else if (payload.command) {
    handleCommand(payload)
  } else if (payload.uiCommand) {
    handleUiCommand(payload)
  } else if (payload.vuiCommand) {
    handleVuiCommand(payload)
  } else if (payload.newSettings) {
    // we ignore settings updates from the web_ui
  } else {
    console.warn('Unhandled payload', payload)
  }
}

const close = () => {
  if (ws !== null) {
    ws.close()
  }
}

const command = (cmd: string) => {
  if (ws !== null) {
    console.log(`command: ${cmd}`)
    state.spin++
    ws.send(JSON.stringify({ command: cmd }))
  } else {
    console.error(`command: ${cmd} called but no websocket`)
  }
}

const uiCommand = (cmd: string, arg: any = null) => {
  if (ws !== null) {
    console.log('uiCommand:', cmd, arg)
    state.spin++
    ws.send(JSON.stringify({ uiCommand: cmd, arg: arg }))
  } else {
    console.error(`uiCommand ${cmd} called but no websocket`)
  }
}

const vuiCommand = (cmd: string, arg: any = null) => {
  if (ws !== null) {
    console.log('vuiCommand:', cmd, arg)
    state.spin++
    ws.send(JSON.stringify({ vuiCommand: cmd, arg: arg }))
  } else {
    console.error(`vuiCommand ${cmd} called but no websocket`)
  }
}

const disconnect = () => {
  if (ws !== null) {
    ws.close()
  } else {
    console.error('disconnect called but no websocket')
  }
  closePreviewWs()
}

const safeCommand = (cmd: string) => {
  if (state.preferences.lbWarning && state.sequence.modified) {
    if (confirm('Sequence has been modified. Discard changes ?')) {
      command(cmd)
    }
  } else {
    command(cmd)
  }
}
const safeUiCommand = (cmd: string, arg: any = null) => {
  if (state.preferences.lbWarning && state.sequence.modified) {
    if (confirm('Sequence has been modified. Discard changes ?')) {
      uiCommand(cmd, arg)
    }
  } else {
    uiCommand(cmd, arg)
  }
}
const safeVuiCommand = (cmd: string, arg: any = null) => {
  if (state.preferences.lbWarning && state.sequence.modified) {
    if (confirm('Sequence has been modified. Discard changes ?')) {
      vuiCommand(cmd, arg)
    }
  } else {
    vuiCommand(cmd, arg)
  }
}

export { close, connect, command, disconnect, safeCommand, safeUiCommand, safeVuiCommand, uiCommand, vuiCommand }
