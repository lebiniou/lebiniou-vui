import { createRouter, createWebHistory } from 'vue-router'
import MainView from '../views/MainView'

const routes = [
  {
    path: '/',
    name: 'Main',
    component: MainView
  },
  {
    path: '/colormaps/:page(\\d+)?',
    name: 'Colormaps',
    component: () => import(/* webpackChunkName: "colormaps" */ '../views/ColormapsView')
  },
  {
    path: '/images/:page(\\d+)?',
    name: 'Images',
    component: () => import(/* webpackChunkName: "images" */ '../views/ImagesView')
  },
  {
    path: '/settings',
    name: 'Settings',
    component: () => import(/* webpackChunkName: "settings" */ '../views/SettingsView')
  },
  {
    path: '/plugins',
    name: 'Plugins',
    component: () => import(/* webpackChunkName: "explorer" */ '../views/ExplorerView')
  },
  {
    path: '/remote',
    name: 'Remote',
    component: () => import(/* webpackChunkName: "remote" */ '../views/RemoteView')
  },
  {
    path: '/documentation',
    name: 'Documentation',
    component: () => import(/* webpackChunkName: "documentation" */ '../views/DocumentationView')
  },
  {
    path: '/hub',
    name: 'Hub',
    component: () => import(/* webpackChunkName: "hub" */ '../views/HubView')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
