import { reactive } from 'vue'

import { State } from '@/state/types'

export const SAMPLES = 3 * 60 // 3'

const state: State = reactive({
  allSequences: [],
  auto: {
    colormaps: {
      value: false,
      mode: 'shuffle',
      range: [15, 30]
    },
    images: {
      value: false,
      mode: 'shuffle',
      range: [15, 30]
    },
    sequences: {
      value: 'Mixed',
      mode: 'shuffle',
      range: [15, 30]
    },
    webcams: {
      value: false,
      mode: 'shuffle',
      range: [15, 30]
    }
  },
  connected: false,
  connection: {
    lbHost: window.location.hostname,
    lbPort: window.location.port === '8080' ? 55555 : parseInt(window.location.port)
  },
  data: {
    colormaps: 0,
    images: 0,
    sequences: 0
  },
  engine: {
    allowAutoColormaps: true,
    allowAutoImages: true,
    currentWebcam: 0,
    fadeDelay: 3000,
    layerModes: [],
    lockColormap: false,
    lockImage: false,
    lockWebcam: false,
    lockedPlugin: '',
    maxFps: 25,
    selectedPlugin: {
      name: '',
      displayName: '',
      parameters: {}
    },
    webcams: 0
  },
  fpsLog: Array(SAMPLES).fill(null),
  input: {
    mute: false,
    name: 'alsa',
    volumeScale: 1.0
  },
  parametersKey: 0,
  params3d: {
    boundary: 'none',
    rotateAmount: 0.001,
    rotateFactor: [50, 50, 50],
    rotations: [0, 0, 0],
    scaleFactorCoeff: 1
  },
  ping: {
    clients: 0,
    currentWebcam: 0,
    fps: 0,
    uptime: 0
  },
  preferences: {
    lbControls: true,
    lbDeveloper: false,
    lbRescale: false,
    lbSequences: true,
    lbThreed: true,
    lbWarning: true
  },
  screen: {
    fixed: false,
    width: 960,
    height: 540,
    fullscreen: false
  },
  sequence: {
    bandpass: [10, 245],
    colormap: '!pm',
    id: 0,
    image: '00-lebiniou.png',
    modified: false,
    name: '(unsaved)',
    params3d: {
      boundary: 'none',
      rotateAmount: 0.001,
      rotateFactor: [50, 50, 50],
      rotations: [0, 0, 0],
      scaleFactorCoeff: 1
    },
    plugins: []
  },
  shortcuts: {
    colormaps: [],
    images: []
  },
  spin: 0,
  versions: {
    lebiniou: '',
    ulfius: ''
  }
})

export default state
