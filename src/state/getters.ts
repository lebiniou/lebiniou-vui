import { State } from '@/state/types'

interface Getters {
  getHttpUrl(state: State): string
  getHubUrl(state: State): string
  getHubWsUrl(state: State): string
  getPreviewUrl(state: State): string
  getWsUrl(state: State): string
  isLockedPlugin(state: State): boolean
  isRotating(state: State): boolean
}

export const getters: Getters = {
  getHttpUrl: state => `http://${state.connection.lbHost}:${state.connection.lbPort}`,
  getHubUrl: state => `http://${state.connection.lbHost}:3000`,
  getHubWsUrl: state => `ws://${state.connection.lbHost}:3000`,
  getPreviewUrl: state => `ws://${state.connection.lbHost}:${state.connection.lbPort}/ui/preview`,
  getWsUrl: state => `ws://${state.connection.lbHost}:${state.connection.lbPort}/ui`,
  isLockedPlugin: state => state.engine.selectedPlugin.name === state.engine.lockedPlugin,
  isRotating: state => {
    const [rotX, rotY, rotZ] = state.params3d.rotateFactor
    return (rotX || rotY || rotZ) !== 0
  }
}

export default getters
