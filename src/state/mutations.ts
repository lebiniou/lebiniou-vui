import state from '@/state'
import { AutoModeName, AutoModeValue, Params3d, Plugin, PluginParameters, Range, RandomSequences, Sequence } from '@/state/types'

const setPreference = (preference: string, value: any) => {
  state.preferences[preference] = value
  localStorage.setItem(preference, JSON.stringify(value))
}

const updateAuto = (auto: {
                            what: AutoModeName,
                            value: boolean | RandomSequences,
                            allow?: boolean,
                            mode: AutoModeValue,
                            range: Range
                          }) => {
  if (typeof auto.value !== 'undefined') {
    if (typeof auto.allow !== 'undefined') {
      if (auto.allow) {
        if (auto.what === 'colormaps') {
          state.engine.allowAutoColormaps = auto.value as boolean
        } else {
          state.engine.allowAutoImages = auto.value as boolean
        }
      } else {
        state.auto[auto.what].value = auto.value
      }
    } else {
      state.auto[auto.what].value = auto.value
    }
  }
  if (auto.mode) {
    state.auto[auto.what].mode = auto.mode
  }
  if (auto.range) {
    state.auto[auto.what].range = auto.range
  }
}

const updateSequence = (sequence: Sequence) => {
  const {
    autoColormaps, autoImages,
    bandpassMin, bandpassMax,
    colormap, image, params3d,
    plugins, name
  } = sequence
  state.auto.colormaps.value = autoColormaps as boolean
  state.auto.images.value = autoImages as boolean
  state.sequence.bandpass = [bandpassMin as number, bandpassMax as number]
  state.sequence.colormap = colormap as string
  state.sequence.image = image as string
  state.sequence.modified = false
  state.sequence.name = name
  state.params3d = state.sequence.params3d = params3d as Params3d
  state.sequence.plugins = plugins as Plugin[]
  state.parametersKey++
  for (const plugin of plugins) {
    if ((plugin.name === state.engine.selectedPlugin.name) && state.engine.selectedPlugin.parameters) {
      state.engine.selectedPlugin.parameters = plugin.parameters
      break
    }
  }
}

const modifySequence = (sequence: Sequence) => {
  updateSequence(sequence)
  state.sequence.modified = true
}

interface SelectedPlugin {
  parameters: PluginParameters
  selectedPlugin: string
  selectedPluginDname: string
}

const updateSelectedPlugin = (plugin: SelectedPlugin) => {
  const { selectedPlugin, selectedPluginDname, parameters } = plugin
  if (selectedPlugin && selectedPluginDname) {
    state.engine.selectedPlugin.name = selectedPlugin
    state.engine.selectedPlugin.displayName = selectedPluginDname
  }
  if (parameters) {
    state.engine.selectedPlugin.parameters = parameters
  } else {
    state.engine.selectedPlugin.parameters = {}
  }
}

interface Changes {
  deleted: string[]
  index: number
  newName: string
  sequence: Sequence
  sequences: number
}

const updateSequences = (changes: Changes) => {
  if (changes.deleted) {
    state.allSequences = state.allSequences.filter(s => !changes.deleted.includes(s.name))
  }
  if (changes.sequence && changes.sequence.id) {
    state.allSequences.unshift({ id: changes.sequence.id, name: changes.sequence.id.toString(), newName: changes.sequence.id.toString(), marked: false, rename: false })
  }
  if (changes.sequences) {
    state.data.sequences = changes.sequences
  }
  if ((typeof changes.index === 'number') && changes.newName) {
    state.allSequences[changes.index].name = changes.newName
  }
}

export { modifySequence, setPreference, updateAuto, updateSequence, updateSequences, updateSelectedPlugin }
