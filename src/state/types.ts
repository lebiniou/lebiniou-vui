type PluginParameterValue = boolean | number | string | string[]

interface PluginParameter {
  type?: 'boolean' | 'integer' | 'double' | 'string_list' | 'playlist'
  value: PluginParameterValue
  min?: number
  max?: number
  step?: number
  description?: string
  // eslint-disable-next-line camelcase
  value_list?: string[]
}

type PluginParameters = {
  [index: string]: PluginParameter
}

interface Plugin {
  name: string
  displayName: string
  enabled: boolean
  favorite: boolean
  lens: boolean
  lensChar: string
  mode: string
  parameters?: PluginParameters
  selected: boolean
}

export interface Ping {
  clients: number
  currentWebcam: number
  fps: number
  uptime: number
}

type Vec3 = [number, number, number]

type Boundary = 'none' | 'cube' | 'sphere_dots' | 'sphere_wireframe'

interface Params3d {
  boundary: Boundary
  rotateAmount: number
  rotateFactor: Vec3
  rotations: Vec3
  scaleFactorCoeff: number
}

interface SequenceItem {
  id: number
  name: string
  newName: string
  marked: boolean
  rename: boolean
}

interface Sequence {
  autoColormaps?: boolean
  autoImages?: boolean
  bandpass: [number, number]
  bandpassMin?: number
  bandpassMax?: number
  colormap: string
  id: number
  image: string
  modified?: boolean
  name: string
  params3d: Params3d
  plugins: Plugin[]
}

type AutoModeName = 'colormaps' | 'images' | 'sequences' | 'webcams'
type AutoModeValue = 'cycle' | 'shuffle' | 'random'
type RandomSequences = 'Off' | 'Schemes' | 'Sequences' | 'Mixed'
type Range = [number, number]

interface AutoModes {
  colormaps: {
    value: boolean
    mode: AutoModeValue
    range: Range
  }
  images: {
    value: boolean
    mode: AutoModeValue
    range: Range
  }
  sequences: {
    value: RandomSequences
    mode: AutoModeValue
    range: Range
  }
  webcams: {
    value: boolean
    mode: AutoModeValue
    range: Range
  }
}

interface Engine {
  allowAutoColormaps: boolean
  allowAutoImages: boolean
  currentWebcam: number
  fadeDelay: number
  layerModes: string[]
  lockColormap: boolean
  lockImage: boolean
  lockWebcam: boolean
  lockedPlugin: string | null
  maxFps: number
  selectedPlugin: {
    name: string
    displayName: string
    parameters?: PluginParameters
  }
  webcams: number
}

interface Input {
  mute: boolean
  name: string | null
  volumeScale: number
}

interface Data {
  colormaps: number
  images: number
  sequences: number
}

type PreferenceType = boolean | string | number
type Preferences = { [index: string]: PreferenceType }

interface Screen {
  fixed: boolean
  width: number
  height: number
  fullscreen: boolean
}

type Shortcut = number | null

interface Shortcuts {
  colormaps: Shortcut[]
  images: Shortcut[]
}

interface Versions {
  lebiniou: string
  ulfius: string
}

interface Connection {
  lbHost: string
  lbPort: number
}

interface State {
  allSequences: SequenceItem[]
  auto: AutoModes
  connected: boolean
  connection: Connection
  data: Data
  engine: Engine
  fpsLog: number[]
  input: Input
  parametersKey: number
  params3d: Params3d
  ping: Ping
  preferences: Preferences
  screen: Screen
  sequence: Sequence
  shortcuts: Shortcuts
  spin: number
  versions: Versions
}

export {
  AutoModeName, AutoModeValue,
  Params3d, Plugin, PluginParameterValue, PluginParameters, PreferenceType,
  RandomSequences, Range,
  Sequence, SequenceItem, Shortcut, State
}
